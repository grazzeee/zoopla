<?php
    $module_options = get_sub_field('module_options');
        $anchor_tag = $contents['anchor_tag'];

    $contents = get_sub_field('contents');
        $slides = $contents['slides'];

        $counter = 0;
        $noSlides = 0;
        
        if($slides) {
            foreach($slides as $slide) {   
                $noSlides++;
            }
        }
    
        if ( $noSlides > 1 ) {
            $carouselState = 'on';
        } else {
            $carouselState = 'off';
        }
?>

<div class="outer-wrap">
    <div class="container center-content-1440">

        <div class="container center-content-full">
            <div data-watchCss="<?php echo $carouselState ?>" class="hero-carousel">

                <?php if($slides) {
                    foreach($slides as $slide) {
                        
                        $mobile_image = $slide['background_images']['mobile_image'];
                        $tablet_image = $slide['background_images']['tablet_image'];
                        $desktop_image = $slide['background_images']['desktop_image'];
                        
                        $mobileRatio = '800x480';
                        $tabletRatio = '1024x432';
                        $desktopRatio = '1440x432';

                        $size = array(
                            array('w' => 500, 'upsize' => true),
                            array('w' => 900),
                            array('w' => 1440),
                        );

                        echo '<div class="slide-container">';
                            echo '<div class="background-container">';
                                include(get_stylesheet_directory() . "/template-parts/parts//background-image-multi.php");
                            echo '</div>';
                        echo '</div>';

                        $counter++;
                    }
                } ?>

            </div>
        </div>
    </div>
</div>