<?php
    $module_options = get_sub_field('module_options');
        $anchor_tag = $contents['anchor_tag'];

    $contents = get_sub_field('contents');
        $content = $contents['content'];
            $title = $content['title'];
            $description = $content['description'];

        $form = $contents['form'];
?>

<div class="outer-wrap">
    <div class="container center-content-1180 columns col-50-50 switch-portrait">

        <div class="column">
            <div class="container-inner">

                <?php if( $title ) { ?>
                    <div class="title-wrap">
                        <span class="h2"><?php echo $title ?></span>
                    </div>
                <?php } ?>

                <?php if( $description ) { ?>
                    <div class="description-wrap">
                        <?php echo apply_filters('the_content', $description); ?>
                    </div>
                <?php } ?>

            </div>
        </div>

        <div class="column">
            <div class="container-inner">
                <div class="form-wrap show-section-1">

                    <div class="title-wrap">
                        <span class="h3">Sign up</span>
                    </div>

                    <div class="description-wrap">
                        <p>Step 1 of 2</p>
                    </div>

                    <?php echo $form; ?>

                </div>
            </div>
        </div>
    </div>
</div>
