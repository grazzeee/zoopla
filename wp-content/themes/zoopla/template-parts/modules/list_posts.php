<?php
    $module_options = get_sub_field('module_options');
        $anchor_tag = $contents['anchor_tag'];

    $contents = get_sub_field('contents');
        $title = $contents['title'];
        $number_of_posts = $contents['number_of_posts'];

    $query = new WP_Query( array(
        'post_status' => 'publish',
        'post_type' => 'post',
        'posts_per_page' => $number_of_posts,
        'order' => 'ASC',
    ));
?>

<div class="outer-wrap">
    <div class="container center-content-1180">

        <?php if( $title ) { ?>
            <div class="container intro">
                <div class="title-wrap">
                    <h2><?php echo $title ?></h2>
                </div>
            </div>
        <?php } ?>

        <div class="container list-posts">

            <?php

                if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post();
                        
                        $title = get_the_title();
                        $intro = get_field('content')['intro'];
                        $image = get_field('media')['desktop'];
                    ?>

                        <div class="post-preview">

                            <div class="background-container normal">
                                <?php 
                                    $ratio = '16x9';
                                    $size = array(
                                        array('w' => 300, 'upsize' => true),
                                        array('w' => 500),
                                    );

                                    include(get_stylesheet_directory() . "/template-parts/parts//background-image-single.php"); 
                                ?>
                            </div>

                            <div class="title-wrap">
                                <h5><?php echo $title ?></h5>
                            </div>

                            <div class="description-wrap">
                                <p><?php echo $intro ?></p>
                            </div>

                        </div>

                    <?php 
                    endwhile;
                    wp_reset_postdata();
                else : endif;

            ?>

        </div>
    </div>
</div>
