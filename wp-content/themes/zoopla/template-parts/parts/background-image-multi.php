<?php

    $mobWidth = substr($mobileRatio, 0, strpos($mobileRatio, 'x'));
    $mobHeight = substr($mobileRatio, strpos($mobileRatio, "x") + 1);

    $tabWidth = substr($tabletRatio, 0, strpos($tabletRatio, 'x'));
    $tabHeight = substr($tabletRatio, strpos($tabletRatio, "x") + 1);

    $deskWidth = substr($desktopRatio, 0, strpos($desktopRatio, 'x'));
    $deskHeight = substr($desktopRatio, strpos($desktopRatio, "x") + 1);

    $mobileImage = $mobile_image['url'];
    $tabletImage = $tablet_image['url'];
    $desktopImage = $desktop_image['url'];

    if ( $tabletImage == '' ) {
        $tabletImage = $desktopImage;
    }

    if ( $mobileImage == '' ) {
        if ( $tabletImage == '' ) {
            $mobileImage = $desktopImage;
        } else {
            $mobileImage = $tabletImage;
        }
    }

    echo '<div class="background-images">';
        echo '<img class="background-mobile fill-container" src="'. aq_resize( $mobileImage, $mobWidth, $mobHeight, true, true, true ) .'" srcset="'. generate_srcset( $mobileImage, $size, $mobileRatio ) .'" alt="background image" />';
        echo '<img class="background-tablet fill-container" src="'. aq_resize( $tabletImage, $tabWidth, $tabHeight, true, true, true ) .'" srcset="'. generate_srcset( $tabletImage, $size, $tabletRatio ) .'" alt="background image" />';
        echo '<img class="background-desktop fill-container" src="'. aq_resize( $desktopImage, $deskWidth, $deskHeight, true, true, true ) .'" srcset="'. generate_srcset( $desktopImage, $size, $desktopRatio ) .'" alt="background image" />';
    echo '</div>';
?>