<?php
    $width = substr($ratio, 0, strpos($ratio, 'x'));
    $height = substr($ratio, strpos($ratio, "x") + 1);
    
    echo '<div class="background-image">';
        echo '<img class="fill-container" src="'. aq_resize( $image['url'], $width, $height, true, true, true ) .'" srcset="'. generate_srcset( $image['url'], $size, $ratio ) .'" alt="background image" />';
    echo '</div>';
?>