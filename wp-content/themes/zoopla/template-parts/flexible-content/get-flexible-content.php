<?php
    // $choose_theme = get_field('choose_theme', 'option');
    echo '<div class="flexible-content-wrap clearfix">';

        // GET ACF FLEXIBLE ROWS
        $flexIndex = 1;
        $module_name = get_field('show_module_name_and_code', 'option');

        $show_module_name = 'hide';
        if ( $module_name == 1 ) {
            $show_module_name = 'show';
        }

        if (have_rows('flexible_content')) :
            while (have_rows('flexible_content')) : the_row();

                echo '<section class="flexible-content module-'.get_row_layout().' row-index-'.$flexIndex.' viewport_check">';

                    echo '<div class="module-name '. $show_module_name .'">';
                        echo 'Module Code: '. get_row_layout();
                    echo '</div>';

                    $anchor = get_sub_field('module_options')['anchor_tag'];

                    if ( $anchor != '' ) {
                        echo '<div class="anchorLink" id="'.$anchor.'"></div>';
                    }

                    $file = get_stylesheet_directory() . '/template-parts/modules/'. get_row_layout() .'.php';
                    include($file);

                echo '</section>';

                $flexIndex++;

            endwhile;
        endif;

        $flex_name = false;

        // IMPORTANT - reset the $post object so the rest of the page works correctly
        wp_reset_postdata(); 
        
    echo '</div>';
?>
