<?php 
    $branding_colours = get_field('branding_colours');
    $branding_colour = $branding_colours['branding_colour'];

    if ( $branding_colour == '') {
        $branding_colour = '#40a6e6';
    }

    $footer = get_field('footer', 'option');
    $left_column = $footer['left_column'];
        $column_title_left = $left_column['column_title'];
        $menu_one = $left_column['menu_one'];
        $menu_two = $left_column['menu_two'];

    $right_column = $footer['right_column'];
        $column_title_right = $right_column['column_title'];
        $column_description = $right_column['column_description'];

        $facebook = get_field('facebook', 'option');
        $instagram = get_field('instagram', 'option');
        $twitter = get_field('twitter', 'option');
        $linkedin = get_field('linkedin', 'option');

    if ( is_single() ) {
        echo '<div style="background-color: '. $branding_colour .'" class="footer-wrap clearfix">';
    } else {
        echo '<div class="footer-wrap clearfix">';
    }
?>
        <div class="container center-content-1280">

            <div class="container social-icons">
                <?php 
                    if ( $facebook ) {
                        echo '<a href="'. $facebook .'" class="carousel-button facebook">';
                            include(get_stylesheet_directory() . "/assets/svg/alc/facebook-logo-f.php");
                        echo '</a>';
                    }

                    if ( $instagram ) {
                        echo '<a href="'. $instagram .'" class="carousel-button instagram">';
                            include(get_stylesheet_directory() . "/assets/svg/alc/instagram.php");
                        echo '</a>';
                    }

                    if ( $twitter ) {
                        echo '<a href="'. $twitter .'" class="carousel-button twitter">';
                            include(get_stylesheet_directory() . "/assets/svg/alc/twitter.php");
                        echo '</a>';
                    }

                    if ( $linkedin ) {
                        echo '<a href="'. $linkedin .'" class="carousel-button twitter">';
                            include(get_stylesheet_directory() . "/assets/svg/ard/linkedin.php");
                        echo '</a>';
                    }
                ?>
            </div>

            <div class="container menu">
                <?php 
                    if( $menu_one ) {
                        wp_nav_menu( array( 'theme_location' => 'footer-menu-one' ) );
                    }
                ?>
            </div>

            <div class="container copyright">
                    <span>©<?php echo date('Y'); ?> Active Learning Centres</span>
            </div>

        </div>
    </div>