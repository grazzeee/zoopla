<div class="header container center-content-full viewport_check">
    <div class="header-inner container center-content-1440">

        <a href="<?php echo get_site_url(); ?>" class="site-logo">
            <?php include(get_stylesheet_directory() . "/assets/svg/zoopla.php"); ?>
            <?php include(get_stylesheet_directory() . "/assets/svg/advantage.php"); ?>
        </a>

    </div>
</div>