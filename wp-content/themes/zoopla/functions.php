<?php
    /*
    * ADVANCED CUSTOM FIELD
    */

    require_once(get_stylesheet_directory() . "/config/acf/module-img-previews.php");

    /*
    * ENQUEUE
    */

    require_once(get_stylesheet_directory() . "/config/enqueue/site-scripts.php");
    require_once(get_stylesheet_directory() . "/config/enqueue/site-styles.php");

    /*
    * FUNCTIONS
    */

    require_once(get_stylesheet_directory() . "/config/functions/animate_characters.php");
    require_once(get_stylesheet_directory() . "/config/functions/aq_resizer.php");
    require_once(get_stylesheet_directory() . "/config/functions/aq_resizer_srcset.php");

    /*
    * CUSTOM THEME SUPPORT
    */

    require_once(get_stylesheet_directory() . "/config/theme/browser-support.php");
    require_once(get_stylesheet_directory() . "/config/theme/media.php");
    require_once(get_stylesheet_directory() . "/config/theme/menus.php");
    require_once(get_stylesheet_directory() . "/config/theme/pages.php");
    require_once(get_stylesheet_directory() . "/config/theme/posts.php");
    require_once(get_stylesheet_directory() . "/config/theme/security.php");
    require_once(get_stylesheet_directory() . "/config/theme/utilities.php");
    require_once(get_stylesheet_directory() . "/config/theme/widgets.php");
    require_once(get_stylesheet_directory() . "/config/theme/wp-core.php");