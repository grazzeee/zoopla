<?php
	/*
    * remove unnessassary wordpress code from site.
    */
	remove_action('wp_head', 'wp_generator');
	remove_action('opml_head', 'the_generator');

	/*
    * update wordpress login error
    */

	function no_wordpress_errors(){
		return 'Login credentials are incorrect.';
	}

	add_filter( 'login_errors', 'no_wordpress_errors' );
?>