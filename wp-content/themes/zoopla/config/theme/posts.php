<?php
	/*
    * Remove default post type from side menu
    */
	// add_action( 'admin_menu', 'remove_default_post_type' );
	// function remove_default_post_type() {
	//     remove_menu_page( 'edit.php' );
	// }
	
	/*
    * Remove default text input fields from pages
    */
	add_action('admin_init', 'remove_textarea');
    function remove_textarea() {
        remove_post_type_support( 'page', 'editor' );
	}
	
?>