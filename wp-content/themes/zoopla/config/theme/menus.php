<?php
	// Register menu locations
	function register_my_menus() {
		register_nav_menus(
			array(
				'main-menu' => __( 'Main Menu' ),
				'footer-menu-one' => __( 'Footer Menu One' ),
				'footer-menu-two' => __( 'Footer Menu Two' ),
			)
		);
	}
	add_action( 'init', 'register_my_menus' );

	/*
    * ADD CLASSES TO MENU
    */
	function my_menu_class($menu) {
		$level = 0;
		$stack = array('0');
		foreach($menu as $key => $item) {
			while($item->menu_item_parent != array_pop($stack)) {
				$level--;
			}   
			$level++;
			$stack[] = $item->menu_item_parent;
			$stack[] = $item->ID;
			$menu[$key]->classes[] = 'level-'. ($level - 1);
		}                    
		return $menu;        
	}
	
	add_filter('wp_nav_menu_objects' , 'my_menu_class');
?>