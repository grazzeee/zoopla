<?php
	/*
    * Flush theme rewrite rules
    */
	add_action( 'after_switch_theme', 'flush_rewrite_rules' );
	register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
	register_activation_hook( __FILE__, 'myplugin_flush_rewrites' );
	function myplugin_flush_rewrites() {
		myplugin_custom_post_types_registration();
		flush_rewrite_rules();
	}
?>