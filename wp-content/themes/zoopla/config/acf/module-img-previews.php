<?php

    add_filter('acfe/flexible/thumbnail/layout=hero', 'cws_hero', 10, 3);
    function cws_hero($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/assets/img/modules/hero.jpg';
    }

    add_filter('acfe/flexible/thumbnail/layout=sign_up', 'sign_up', 10, 3);
    function sign_up($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/assets/img/modules/sign-up.jpg';
    }

    add_filter('acfe/flexible/thumbnail/layout=list_posts', 'list_posts', 10, 3);
    function list_posts($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/assets/img/modules/list-posts.jpg';
    }
    
?>