<?php

    // Load styles
    function enqueue_styles() {
        
        wp_enqueue_style('plugin-styles', get_template_directory_uri() . '/assets/css/plugin-styles.min.css', array(), filemtime(get_template_directory() . '/assets/css/plugin-styles.min.css'), false);
        wp_enqueue_style('plugin-styles');

        wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/styles.css', array(), filemtime(get_template_directory() . '/assets/css/styles.min.css'), false);
        wp_enqueue_style('styles');

    }

    add_action('wp_enqueue_scripts', 'enqueue_styles');
    

    // ADMIN STYLES
    function enqueue_custom_admin_styles() {
        wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/styles-admin.min.css', array(), filemtime(get_template_directory() . '/assets/css/styles-admin.min.css'), false);
        wp_enqueue_style( 'custom_wp_admin_css' );
    }

    add_action( 'admin_enqueue_scripts', 'enqueue_custom_admin_styles' );
?>