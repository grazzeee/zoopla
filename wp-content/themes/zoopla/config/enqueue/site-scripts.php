<?php
    // Load header scripts
    function header_scripts() {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

            wp_deregister_script('jquery');

            wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), filemtime(get_template_directory() . '/assets/js/jquery.min.js'), false);
            wp_enqueue_script('jquery');

            wp_register_script('conditionizr', get_template_directory_uri() . '/assets/js/conditionizr.js', array(), filemtime(get_template_directory() . '/assets/js/conditionizr.js'), false);
            wp_enqueue_script('conditionizr');

            wp_register_script('libraries', get_template_directory_uri() . '/assets/js/libs.min.js', array(), filemtime(get_template_directory() . '/assets/js/libs.min.js'), false);
            wp_enqueue_script('libraries');

            wp_register_script('loadscripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), filemtime(get_template_directory() . '/assets/js/scripts.min.js'), false);
            wp_enqueue_script('loadscripts');
        }

    }

    add_action('init', 'header_scripts');

?>