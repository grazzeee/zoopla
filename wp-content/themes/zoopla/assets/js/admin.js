/*!
 * zoopla v0.1.0
 * Wordpress Setup & Theme boilerplate
 * (c) 2022 Graeme Wetenhall
 * MIT License
 * https://bitbucket.org/grazzeee/
 */

(function($) {
    $(document).ready((function () {

        // ADD LOGO TO MENU
        $('<li class="menu-top menu-top-first menu-icon-menu" id="menu-menu"><a href="nav-menus.php" class="wp-first-item menu-top menu-top-first menu-icon-menu menu-top-first" aria-haspopup="false"><div class="wp-menu-arrow"><div></div></div><div class="wp-menu-image dashicons-before dashicons-menu"><br></div><div class="wp-menu-name">Menus</div></a></li>')
        .insertAfter('#adminmenu li:eq(0)');
        
        // MOVE ADMIN MENU LINK INTO MENU
        if ( $('.branding-site-link')[0] ) {
            $('.branding-site-link').insertBefore('#adminmenu li:eq(0)');
        }

        // Remove SlugDiv Fields
        if ( $('#slugdiv')[0] ) {
            $('#slugdiv').remove();
        }

        // CLOSE SEO YOAST ON LOAD
        $('#wpseo-dashboard-overview').addClass('closed');

        // FIX YOAST ICON
        $('#yoast-ab-icon').css('background-image','unset');

        // DASHBOARD ADMIN PANNEL
        $('#wp-custom-panel ').insertAfter('.wrap h1');

        // DASHBOARD ADMIN PANNEL
        $('#wp-custom-welcome').insertAfter('.wrap h1');

        // SHOW DASHBOARD FADE IN
        setTimeout((function(){
            $('#wp-custom-welcome').addClass('show');
            $('#wp-custom-panel ').addClass('show');
            $('#dashboard-widgets-wrap').addClass('show');
        }), 500);

        // SET IFRAME HEIGHT
        // if ($('.toplevel_page_video-tutorials')[0]) {
        //     page_height = $( '#adminmenu' ).height();            
        //     $('#iframe-container').css('height', page_height - 100 );
        // }

        //remove users so they cant be deleted
        // if ($('.users-php')[0]) {
        //     $('.wp-list-table.users tbody tr').each(function(){
        //         var getName = $(this).children('.username').children('strong').children('a').html();
        
        //         if ( getName == 'graeme' || getName == 'james' || getName == 'James' || getName == 'Rebecca' || getName == 'rebecca' || getName == 'blend-digital' ) {
        //             $(this).children('.username').children('.row-actions').children('.delete').remove();
        //         }
                
        //     });
        // }

    }));
})(jQuery);