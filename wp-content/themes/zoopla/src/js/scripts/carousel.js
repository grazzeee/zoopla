$(function() {

	$('.hero-carousel').flickity({
		prevNextButtons: true,
		watchCSS: true,
		pageDots: false,
		fade: true,
		freeScroll: true,
		wrapAround: true,
	});

});