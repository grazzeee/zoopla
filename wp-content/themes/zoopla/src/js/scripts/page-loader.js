$(function() {
	
	// Reveal page on loading
	function pageloaded() {
		$('html').addClass('page-loaded');
	}
	
	pageloaded();

	// Fix back and forward browser navigation
	window.addEventListener( "pageshow", function ( event ) {
		var historyTraversal = event.persisted || 
							   ( typeof window.performance != "undefined" && 
									window.performance.navigation.type === 2 );
		if ( historyTraversal ) {
		  // Handle page restore.
		  window.location.reload();
		}
	});

});