$(function() {

	var checkView = $('.viewport_check:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');
		
		this.destroy();
	}, {
		offset: '90%'
	});

});