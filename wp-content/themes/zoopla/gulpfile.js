/**
 * Theme Settings
 * This will be the name of the wordpress theme folder
 */

var themeName = 'zoopla';

/**
 * Theme Paths
 * This will be the file locations
 */

var paths = {
	input: 'src/',
	output: 'dist/',
	scripts: {
		input: 'src/js/*',
		output: 'assets/js/'
	},
	libraryScripts: {
		input: 'src/jquery/*',
		output: 'assets/js/'
	},
	styles: {
		input: 'src/sass/styles.scss',
		output: 'assets/css/'
	},
	adminStyles: {
		input: 'src/sass-admin/styles.scss',
		output: 'assets/css/'
	},
	pluginStyles: {
		input: 'src/css/**/*',
		output: 'assets/css/'
	},
};

/**
 * Template for banner to add to file headers
 */

var banner = {
	full:
		'/*!\n' +
		' * <%= package.name %> v<%= package.version %>\n' +
		' * <%= package.description %>\n' +
		' * (c) ' + new Date().getFullYear() + ' <%= package.author.name %>\n' +
		' * <%= package.license %> License\n' +
		' * <%= package.repository.url %>\n' +
		' */\n\n',
	min:
		'/*!' +
		' <%= package.name %> v<%= package.version %>' +
		' | (c) ' + new Date().getFullYear() + ' <%= package.author.name %>' +
		' | <%= package.license %> License' +
		' | <%= package.repository.url %>' +
		' */\n'
};


/**
 * Gulp Packages
 */

// General
var {gulp, src, dest, watch, series, parallel} = require('gulp');
var del = require('del');
var flatmap = require('gulp-flatmap');
var lazypipe = require('lazypipe');
var rename = require('gulp-rename');
var header = require('gulp-header');
var package = require('./package.json');
var plumber = require('gulp-plumber');
var watch = require('gulp-watch');

// Scripts
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var optimizejs = require('gulp-optimize-js');

// Styles
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var minify = require('gulp-cssnano');
var cleanCSS = require('gulp-clean-css');

// SVGs
var svgmin = require('gulp-svgmin');

/**
 * Gulp Tasks
 */

// Repeated JavaScript tasks
var jsTasks = lazypipe()
	.pipe(header, banner.full, {package: package})
	.pipe(optimizejs)
	.pipe(dest, paths.scripts.output)
	.pipe(rename, {suffix: '.min'})
	.pipe(uglify)
	.pipe(optimizejs)
	.pipe(header, banner.min, {package: package})
	.pipe(dest, paths.scripts.output);

// Lint, minify, and concatenate scripts
var buildScripts = function (done) {

	// Run tasks on script files
	src(paths.scripts.input)
		.pipe(plumber())
		.pipe(flatmap(function(stream, file) {

			// If the file is a directory
			if (file.isDirectory()) {

				// Setup a suffix variable
				var suffix = '';

				// Grab all files and concatenate them
				// If separate polyfills enabled, this will have .polyfills in the filename
				src(file.path + '/*.js')
					.pipe(concat(file.relative + suffix + '.js'))
					.pipe(jsTasks());

				return stream;

			}

			// Otherwise, process the file
			return stream.pipe(jsTasks());

		}));

	// Signal completion
	done();

};

// // Lint, minify, and concatenate scripts
var buildLibraryScripts = function (done) {

	// Run tasks on script files
	src(paths.libraryScripts.input)
		.pipe(plumber())
		.pipe(flatmap(function(stream, file) {

			// If the file is a directory
			if (file.isDirectory()) {

				// Setup a suffix variable
				var suffix = '';

				// Grab all files and concatenate them
				// If separate polyfills enabled, this will have .polyfills in the filename
				src(file.path + '/*.js')
					.pipe(concat(file.relative + suffix + '.js'))
					.pipe(jsTasks());

				return stream;

			}

			// Otherwise, process the file
			return stream.pipe(jsTasks());

		}));

	// Signal completion
	done();

};

// THEME STYLES
var buildStyles = function (done) {

	// Run tasks on all Sass files
	src(paths.styles.input)
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'expanded',
			sourceComments: true
		}))
		.pipe(prefix({
			overrideBrowserslist: ['last 2 version', '> 0.25%'],
			cascade: true,
			remove: true
		}))
		.pipe(header(banner.full, { package : package }))
		.pipe(dest(paths.styles.output))
		.pipe(rename({suffix: '.min'}))
		.pipe(minify({
			discardComments: {
				removeAll: true
			}
		}))
		.pipe(header(banner.min, { package : package }))
		.pipe(dest(paths.styles.output));

	// Signal completion
	done();

};

// THEME ADMIN STYLES
var buildAdminStyles = function (done) {

	// Run tasks on all Sass files
	src(paths.adminStyles.input)
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'expanded',
			sourceComments: true
		}))
		.pipe(prefix({
			overrideBrowserslist: ['last 2 version', '> 0.25%'],
			cascade: true,
			remove: true
		}))
		.pipe(header(banner.full, { package : package }))
		.pipe(dest(paths.adminStyles.output))
		.pipe(rename({suffix: '-admin.min'}))
		.pipe(minify({
			discardComments: {
				removeAll: true
			}
		}))
		.pipe(header(banner.min, { package : package }))
		.pipe(dest(paths.styles.output));

	// Signal completion
	done();

};

// Process css files
var buildPluginStyles = function (done) {

	// Run tasks on all CSS files
	src(paths.pluginStyles.input)
		.pipe(cleanCSS({
				debug: true,
				compatibility: 'ie8',
				level: {
						1: {
								specialComments: 0,
						},
				},
		}))
		.pipe(concat('plugin-styles.min.css'))
		.pipe(dest(paths.styles.output))

	// Signal completion
	done();

};

// Watch for changes
var watchSourceAssets = function (done) {
	watch(paths.input, series(exports.default));
	done();
};

/**
 * Default Tasks
 * List of default tasks needed to reload the theme after changes
 */

exports.default = series(
	parallel(
		buildScripts,
		buildStyles,
		buildAdminStyles,
	)
);

/**
 * Watch and reload theme files
 * Watch files for changes and reload /dist
 * Clean theme folder
 * Reload js, css, svgs
 * Reload theme files
 */

exports.watch = series(
	watchSourceAssets,
);


exports.buildTheme = series(
	parallel(
		buildLibraryScripts,
		buildPluginStyles,
	)
);