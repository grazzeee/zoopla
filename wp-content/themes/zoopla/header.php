<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php wp_title(''); ?></title>
		<?php wp_head(); ?>
	</head>

	<body <?php body_class( $bodyClasses ); ?>>

		<?php include(get_stylesheet_directory() . "/template-parts/body/header.php"); ?>

		<!-- OPEN MAIN -->
		<main class="main clearfix">