<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'zoopla' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%,yCt*BD@ ~7(S}<yO]Sb`z&cBY|jOY`~3[_K.Iz-}24j#VnQTF=]S8hV&Dn[V9V' );
define( 'SECURE_AUTH_KEY',  'te_z/Hs+G_24eN)c$ZARg|bsprywaIP#(`&{Etrybb./+%n#Ixc(S$-`+zo1PSKt' );
define( 'LOGGED_IN_KEY',    'FF=_FQ{:(K{E60]6?e6!:#3i{%}8`oBZO2(sc57/42i<9e6 <]4[@7 R?pQE?CcM' );
define( 'NONCE_KEY',        '~[{d9Aae6K6HFelJC4*hcy5tMWw|lP?xmhW{HX*HW/n [aC-Vga,-133Qtn)1a.7' );
define( 'AUTH_SALT',        'rEYthwGEuTn{|1aZQKL.mM-VmZrOfp.sSEDn8zI#aQH(DkP8R*-rUFD]Po~@Kw,(' );
define( 'SECURE_AUTH_SALT', '%O9K=?8Qy]w|,Sp%G:hAGhSAC.Q;zxd~oL(l^+>tWGza1(j?6>pPs]GM,gRv6 9Q' );
define( 'LOGGED_IN_SALT',   'aiiAJ[Tm B@ina1hDXzR}5=OVY+y)8{{Y(Tlm].q2@F9[1v?6N1^s gyCkbq]DY ' );
define( 'NONCE_SALT',       '0$l7caJEqsT7-Cq?[xU(&.$!`W]H yQur<Z8:|UuF5SN>;0#yhB`q2~yF,8_))|b' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
