**Graeme Wetenhall - Zoopla theme demo**

I created this theme for the Front end developer task set by Zoopla

---

## The project consists of

1. Wordpress Installation
2. Wordpress Responsive Custom Theme *zoopla*
3. A single page *home - Wordpress Front Page* built from the Figma design supplied

---

## About the build

The work has been uploaded to a Bitbucket Repo: https://bitbucket.org/grazzeee/zoopla/

I would not normally include all the files for Wordpress install into my repo but for the sake of an easy installation your end iv included themn all. 
My local URL was set to zoopla.local so if you can replicate this you should not need to update the database on installation

1. Clone Repo: https://bitbucket.org/grazzeee/zoopla/
2. Set up MAMP or any local server environment that you use and point it to the root folder in the repo. This should contian all the wordpress core files allowing for Wordpress to start
3. Site database is included within the repo in the 'db_backup' folder. Install this to your PHPMYADMIN and link up within the WP_CONFIG File. 
4. Wordpress admin login - User: zoopla 
5. Wordpress admin login - Pass: zoopla

---

## About the theme

I created my theme around the idea of allowing content editors to create custom pages from a set of predefined components rather than creating a page layout that is fixed and unable to be changed. 
If you go to the home page you will see a page layout that allows for modules to be added, edited and modified. 

The modules can be added to the page by clicking 'add row' at the bottom of the page. 
A popup modal should appear allowing for the editor to choose a component to add to the page.
Once added the user can click on the image on the page and edit modal should appear allowing the editor to add text and images to the compenent.
Once added to the page the components can be moved up and down the page to allow full customisation of the page.

1. Custom Theme - Zoopla
2. ACF Plugin used to create the custom fields needed to edit / create components onto the page.
3. Theme /root is laid out in a specific way to how I usually work
4. Gulp is used to minify all the JS/SASS. 
5. To use gulp navigate to the project root and run the following
6. $ npm install
7. $ gulp watch
8. The theme should already have all the needed files in the correct location to run the site as I did not include the build process into the repo for ease/speed of installation
